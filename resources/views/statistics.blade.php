@extends('layouts.app')

@section('content')
<div class="col-md-12">

    <div class="card margin-top-15">
        <h3 class="card-header">Statistics</h3>
        <div class="card-body">
            <div class="form-group">
                <label for="year">Select year</label>
                <select class="form-control" id="year">
                    <option selected disabled>Select year</option>
                </select>
            </div>

            <div class="form-group">
                <label for="month">Select month</label>
                <select class="form-control" id="month">
                    <option selected disabled>Select month</option>
                </select>
            </div>

            <div class="form-group">
                <label for="day">Select day</label>
                <select class="form-control" id="day">
                    <option selected disabled>Select day</option>
                </select>
            </div>
        </div>
    </div>

    <div  id="content">

    </div>

    <div id="graph" style="height: 500px;">

    </div>

</div>
@stop

@section('js')
    <script>
        $(document).ready(function () {
            init();
        });

        function init() {
            let day = $('#day'),
                yearSelect = $('#year'),
                year = new Date().getFullYear(),
                month = $('#month');

            let months = ['Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'];

            let data = {};

            for (let x = 1; x <= 4; x++) {
                yearSelect.append(
                    $('<option></option>')
                        .attr('value', year)
                        .text(year)
                );
                year--;
            }

            for (let y = 1; y <= months.length; y++) {
                month.append(
                    $('<option></option>')
                        .attr('value', y)
                        .text(months[y - 1])
                );
            }

            sendQuery(data);

            month.on('change', function () {
                $('#content').empty();
                day.find('option').remove();

                data = {
                    ...data,
                    month: $(this).val()
                };

                let selectedYear = yearSelect.find(':selected').val();
                let lastDay = 31;

                switch ($(this).val()) {
                    case '2':
                        lastDay = parseInt(selectedYear) % 4 === 0 ? 29 : 28;
                        break;
                    case '4':
                    case '6':
                    case '9':
                    case '11':
                        lastDay = 30;
                        break;
                    default:
                        lastDay = 31;
                }

                day.append(
                    $('<option selected disabled></option>')
                        .text('Select day')
                );
                for (let i = 1; i <= lastDay; i++) {
                    day.append(
                        $('<option></option>')
                            .attr('value', i)
                            .text(i)
                    );
                }

                sendQuery(data);
            });

            yearSelect.on('change', function () {
                $('#content').empty();

                data = {
                    ...data,
                    year: $(this).val()
                };

                sendQuery();
            });

            day.on('change', function () {
                $('#content').empty();

                data = {
                    ...data,
                    day: $(this).val()
                };

                sendQuery(data);
            });
        }

        function sendQuery(data) {
            $.ajax({
                type: 'get',
                data: {
                    ...data
                },
                url: '{{ route('statistics.data') }}',
                success: function (response) {

                    $('#graph').empty();
                    createGraph(response.time);

                    $.each(response.timers, function (key, value) {

                        let totalSeconds = value.time;
                        let hours = Math.floor(totalSeconds / 3600);
                        totalSeconds %= 3600;
                        let mins = Math.floor(totalSeconds / 60);
                        let seconds = totalSeconds % 60;

                        $('#content').append(
                            '<div class="card border-info margin-top-15">' +
                            '    <h3 class="card-header">'+ value.title +'</h3>' +
                            '    <div class="card-body">' +
                            '        <ul class="list-group list-group-flush">' +
                            '            <li class="list-group-item">' +
                            '                <div class="row">' +
                            '                    <div class="col-md-12">' +
                            '                        '+ hours + ' Hours | ' + mins + ' mins | ' + seconds + ' seconds' +
                            '                    </div>' +
                            '                </div>' +
                            '            </li>' +
                            '        </ul>' +
                            '    </div>' +
                            '    <div class="card-footer text-muted">'+ value.started_at +'</div>'+
                            '</div>'
                        );

                    });
                }
            });
        }

        function createGraph(data) {

            let labels = [];
            let series = [];

            $.each(data, function (k, i) {
                labels.push(
                    k
                );
                series.push(
                    i.time
                );
            });

            let rangeData = {
                labels,
                series: [series]
            };

            let options = {
                horizontalBars: true,
                axisY: {
                    offset: 80
                },
                axisX: {
                    scaleMinSpace: 100,
                    labelInterpolationFnc: function(value) {
                        return (value / 3600).toFixed(2);
                    },
                }
            };


            new Chartist.Bar('#graph', rangeData, options);
        }
    </script>
@stop
