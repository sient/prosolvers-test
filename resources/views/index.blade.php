@extends('layouts.app')

@section('content')
<div class="col-md-12">

    <div class="card margin-top-15">
        <h3 class="card-header">Tracker</h3>
        <div class="card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="row">
                        @if ($running)
                            <div class="col-md-6" id="text">
                                Calculation...
                            </div>

                            <div class="col-md-6 text-right">
                                <a href="{{ route('timer.stop', ['id' => $running->id]) }}" type="button" class="btn btn-outline-danger btn-sm">Stop</a>
                            </div>
                        @else
                            <div class="col-md-6">
                                <form action="{{ route('timer.start') }}" id="form" method="POST">
                                    {{ csrf_field() }}

                                    <input type="text" name="title" class="form-control" placeholder="Title" required>
                                </form>
                            </div>

                            <div class="col-md-6 text-right">
                                <button id="start" type="button" class="btn btn-outline-info btn-sm">Start</button>
                            </div>
                        @endif
                    </div>
                </li>
            </ul>
        </div>
    </div>

    @foreach ($timers as $timer)
    <div class="card border-info margin-top-15">
        <h3 class="card-header">{{ $timer->title }}</h3>
        <div class="card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-md-12">
                            Time: {{ $timer->timer->h . ' Hours | ' . $timer->timer->i . ' mins | ' . $timer->timer->s . ' seconds'}}
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="card-footer text-muted">
            Started at: {{ $timer->started_at }} Stopped at: {{ $timer->stopped_at }}
        </div>
    </div>
    @endforeach

    <div class="text-center margin-top-15">
        {{ $timers->links() }}
    </div>

</div>
@stop

@section('js')
    <script>
        $('#start').on('click', function () {
            $('form#form').submit();
        })
    </script>

    <script>
        let start = null;

        @if ($running)
            start = new Date('{{ $running->created_at }}');
        @endif

        function timer() {
            let startStamp = start.getTime();

            let now = new Date();
            let nowStamp = now.getTime();

            let diff = Math.round((nowStamp - startStamp) / 1000);

            let hours = Math.floor(diff / (60 * 60));
            diff -= (hours * 60 * 60);
            let minutes = Math.floor(diff / 60);
            diff -= minutes * 60;
            let seconds = diff;

            $('#text').text(hours + ' Hours | ' + minutes + ' mins | ' + seconds + ' seconds');
        }

        if (start) {
            setInterval(timer, 1000);
        }
    </script>
@stop
