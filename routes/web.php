<?php

Route::get('/', 'HomeController@index')->name('home');
Route::get('statistics', 'StatisticsController@index')->name('statistics');
Route::get('statistics/data', 'StatisticsController@getData')->name('statistics.data');

Route::prefix('timer')->group(function () {
    Route::post('start', 'HomeController@store')->name('timer.start');
    Route::get('stop/{id}', 'HomeController@update')->name('timer.stop');
});
