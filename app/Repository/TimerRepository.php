<?php

namespace App\Repository;

use App\Timer;
use Carbon\Carbon;

class TimerRepository implements TimeRepositoryInterface
{
    protected $timer;

    public function __construct(Timer $timer)
    {
        $this->timer = $timer;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->timer->where('id', $id)->firstOrFail();
    }

    /**
     * @param int $id
     * @param array $data
     * @return Timer
     */
    public function update($id, array $data)
    {
        $timer = $this->get($id);
        $timer->update($data);

        return $timer;
    }

    /**
     * @param array $data
     * @return Timer
     */
    public function create(array $data)
    {
        $timer = $this->timer->whereNull('stopped_at')->first();

        if (!$timer) {
            $now = Carbon::now();

            $data['started_at'] = $now;

            $timer = $this->timer->create($data);
        }

        return $timer;
    }

    /**
     * @return Timer
     */
    public function running()
    {
        return $this->timer->whereNull('stopped_at');
    }

    /**
     * @return mixed
     */
    public function stopped()
    {
        return $this->timer->whereNotNull('stopped_at');
    }
}
