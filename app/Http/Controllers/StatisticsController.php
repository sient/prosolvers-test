<?php

namespace App\Http\Controllers;

use App\Http\Requests\StatisticsRequest;
use App\Services\PaginateTimer;

class StatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('statistics');
    }

    /**
     * @param StatisticsRequest $request
     * @return mixed
     */
    public function getData(StatisticsRequest $request)
    {
        $timers = app(PaginateTimer::class)->execute($request->validated());

        return $timers;
    }
}
