<?php

namespace App\Http\Controllers;

use App\Http\Requests\TimerStoreRequest;
use App\Repository\TimerRepository;
use Carbon\Carbon;

class HomeController extends Controller
{
    private $timer;

    /**
     * HomeController constructor.
     *
     * @param TimerRepository $timer
     */
    public function __construct(TimerRepository $timer)
    {
        $this->timer = $timer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $running = $this->timer->running();
        $timers = $this->timer->stopped()->paginate(5);

        foreach ($timers as $t) {
            $t->timer = $t->started_at->diff($t->stopped_at);
        }

        return view('index')->with([
            'running'  => $running->first(),
            'timers' => $timers
        ]);
    }

    /**
     * @param TimerStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TimerStoreRequest $request)
    {
        $data = $request->validated();
        $this->timer->create($data);

        return back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id)
    {
        $now = Carbon::now();
        $data = [
            'stopped_at' => $now
        ];
        $this->timer->update($id, $data);

        return back();
    }
}
